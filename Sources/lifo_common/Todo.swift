//
//  Todo.swift
//  lifo_common
//
//  Created by Jon Sturk on 2019-04-14.
//

import Foundation

public final class Todo: Codable {
	public var id: Int?
	public var title: String
	public var createdAt: Date?
	public var body: String
	public var internalState: Int?
	
	/// Creates a new `Todo`.
	public init(id: Int? = nil, title: String, body: String, internalState: Int = 1) {
		self.id = id
		self.title = title
		self.body = body
		self.internalState = internalState
	}
}
