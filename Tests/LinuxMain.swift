import XCTest

import lifo_commonTests

var tests = [XCTestCaseEntry]()
tests += lifo_commonTests.allTests()
XCTMain(tests)
