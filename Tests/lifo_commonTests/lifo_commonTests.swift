import XCTest
@testable import lifo_common

final class lifo_commonTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(lifo_common().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
